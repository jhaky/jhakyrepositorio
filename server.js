require('dotenv').config();
const express = require('express');//referencia al paquete express
const userFile = require('./user.json');
const cuentaFile = require('./account.json');
const bodyParser = require('body-parser');
const requestJSON = require('request-json');
//Codigo para que reconozca localmente el servicio
var cors = require('cors');

var app = express();

//Codigo para que reconozca localmente el servicio
app.use(cors());
//app.options('*',cors());

app.use(bodyParser.json());
var port = process.env.PORT || 3000;
//const PORT = 3000;
const URL_BASE = '/api-peru/v1/';
const mLabURLbase = 'https://api.mlab.com/api/1/databases/techu11db/collections/';
const apiKey = 'apiKey=NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF';
const apikeyMLab = 'apiKey=' + process.env.MLAB_APIKEY;

//Operacion GET todos los usuarios(Collections) (users.json)'
app.get (URL_BASE + 'userss',
    function(request,response){
      response.status(200);
      response.send(userFile);
    });

app.get (URL_BASE + 'cuentas',
    function(request,response){
      response.status(200);
      response.send(cuentaFile);
    });
//codigo de ezequiel
// GET users a través de mLab
/*app.get(URL_BASE + 'users',
  function(req, res) {
    const httpClient = requestJSON.createClient(mLabURLbase);
    console.log("Cliente HTTP mLab creado.");
    const fieldParam = 'f={"_id":0}&';
   httpClient.get('user?' + fieldParam+ apiKey,
      function(err, respuestaMLab, body) {
        console.log('Error: ' + err);
        console.log('Respuesta MLab: ' + respuestaMLab);
        console.log('Body: ' + body);
        var response = {};
        if(err) {
            response = {"msg" : "Error al recuperar users de mLab."}
            res.status(500);
        } else {
          if(body.length > 0) {
            response = body;
          } else {
            response = {"msg" : "Usuario no encontrado."};
            res.status(404);
          }
        }
        res.send(response);
      }); //httpClient.get(
});

*/
//Operacion GET a un usuario con ID (Instance)'
app.get (URL_BASE + 'userss/:id',
    function(request,response){
      let indice = request.params.id;
      let respuesta =
      (userFile[indice-1] == undefined)?{"msj":"No existe"}:userFile[indice-1]
      let indice2 = request.params.id2;
      console.log(indice);
      console.log(indice2);
      response.status(200).send(respuesta);
    });

    app.get (URL_BASE + 'cuentas/:id',
        function(request,response){
          let indice = request.params.id;
          let respuesta =
          (cuentaFile[indice-1] == undefined)?{"msj":"No existe"}:cuentaFile[indice-1]
          let indice2 = request.params.id2;
          console.log(indice);
          console.log(indice2);
          response.status(200).send(respuesta);
        });
//Ejemplo ezequiel
// Petición GET id con mLab
/*
app.get(URL_BASE + 'users/:id',
  function (req, res) {
    console.log("GET /users/:id");
    console.log(req.params.id);
    var id = req.params.id;
    var queryString = 'q={"userID":' + id + '}&';
    var queryStrField = 'f={"_id":0}&';
    var httpClient = requestJSON.createClient(mLabURLbase);
    httpClient.get('user?' + queryString + queryStrField + apiKey,
      function(err, respuestaMLab, body){
        console.log("Respuesta mLab correcta.");
      //  var respuesta = body[0];
        var response = {};
        if(err) {
            response = {"msg" : "Error obteniendo usuario."}
            res.status(500);
        } else {
          if(body.length > 0) {
            response = body;
          } else {
            response = {"msg" : "Usuario no encontrado."}
            res.status(404);
          }
        }
        res.send(response);
      });
});

//Operacion POST a users
app.post(URL_BASE + 'users',
    function(request,response){
      console.log(request.body);
      console.log(request.body.id);
      let total=userFile.length;
      console.log(total);
      let newUser = {
        id : ++total,
        first_name : request.body.first_name,
        last_name : request.body.last_name,
        email : request.body.email,
        password : request.body.password
      }
      userFile.push(newUser);
      response.status(201);
      response.send({"mensaje": "usuario creado correctamente",
                      "usuario": newUser,
                      "userFile actualizado": userFile});
    }
);
*/
//******3ER ENTREGABLE*************
//LOGIN - LOGOUT user.json
  app.post(URL_BASE + 'login',
    function(request, response) {
    // console.log("POST /apitechu/v1/login");
     console.log(request.body.dni);
     console.log(request.body.password);
     var user = request.body.dni;
     var pass = request.body.password;
     for(us of userFile) {
      if(us.dni == user) {
       if(us.password == pass) {
        us.logged = true;
        writeUserDataToFile(userFile);
        console.log("Login correcto!");
        //response.send({"msg" : "Login correcto.",
        //         "id" : us.id,
        //         "logged" : "true"});
        response.send(us);
       } else {
        console.log("Login incorrecto.");
        response.send({"msg" : "Login incorrecto."});
       }
      }else {
       console.log("Login incorrecto.");
       response.send({"msg" : "Login incorrecto."});
      }
     }
   });

   // LOGOUT - users.json
  app.post(URL_BASE + 'logout',
   function(request, response) {
    //console.log("POST /apitechu/v1/logout");
    var userId = request.body.id;
    for(us of userFile) {
     if(us.id == userId) {
      if(us.logged) {
       delete us.logged; // borramos propiedad 'logged'
       writeUserDataToFile(userFile);
       console.log("Logout correcto!");
       response.send({"msg" : "Logout correcto.", "idUsuario" : us.id});
      } else {
       console.log("Logout incorrecto.");
       response.send({"msg" : "Logout incorrecto."});
      }
     }
    }
  });

  function writeUserDataToFile(data) {
   var fs = require('fs');
   var jsonUserData = JSON.stringify(data);
   fs.writeFile("./user.json", jsonUserData, "utf8",
    function(err) { //función manejadora para gestionar errores de escritura
     if(err) {
      console.log(err);
     } else {
      console.log("Datos escritos en 'user.json'.");
     }
    })
  };

//******2DO ENTREGABLE*************
//Operacion PUT a users
app.put(URL_BASE + 'userss/:id',
 function(req, res){
   console.log(req.body);
   console.log(req.body.id);
   let indice = req.params.id;
   let newUser = {
     id: indice,
     dni: req.body.dni,
     first_name: req.body.first_name,
     last_name: req.body.last_name,
     email: req.body.email,
     password: req.body.password
   }
   userFile[indice-1]= newUser;
   res.status(201);
   res.send({"mensaje":"Usuario actualizado correctamente",
             "usuario":req.body.id,
             "userFile actualizado":userFile[indice-1]});
});
/*
app.delete(URL_BASE + 'users/:id',
  function(request,response){
      let indice=request.params.id;
      let respuesta =
        (userFile[indice-1]== undefined)?
        {"msj":"No existe"}:{"Usuario Eliminado":(userFile.splice(indice-1,1))};
      response.status(200).send(respuesta);
  });

    //Operacion GET a un usuario con ID (Instance)'
app.get (URL_BASE + 'users/:id/:id2',
    function(request,response){
      let indice = request.params.id;
      let indice2 = request.params.id2;
      console.log(indice);
      console.log(indice2);
      response.status(200);
      response.send(userFile[indice-1]);
    });
app.get (URL_BASE + 'usersq',
    function(request,response){
      console.log(request.query);
      let query = request.query;
      console.log(query.max);
      var temp = [];
      for (var i=0;i<query.max; i++){
        temp.push(userFile[i]);
      }
      response.send(temp);
      response.status(200);
    });
app.get (URL_BASE + 'userstotal',
    function(request,response){
      var tot_elem=userFile.length;
      var total = {
          "num_elem" : tot_elem
      }
      console.log(request.query);
      response.send(total);
      response.status(200);
    });
app.get (URL_BASE + 'userstotalprofe',
    function(request,response){
      console.log(request.query);
      let total=userFile.length;
      let totalJson = JSON.stringify({num_elem : total});
      response.send(totalJson);
      response.status(200);
    });
//Servidor escucharà en la url (servidor local)
//http://localhost:3000/holamundo
*/
app.listen(port, function(){
    console.log('Node JS escuchando en el puerto Bricky...');
});
